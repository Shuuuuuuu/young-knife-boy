﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject currentCheckpoint;
    private Player player;

    void Start()
    {
        player = FindObjectOfType<Player>();
    }
    public void RespawnPlayer()
    {
        player.transform.position = currentCheckpoint.transform.position;
    }
}
