﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    //Variables
    public float maxSpeed = 3;
    public float speed = 50f;
    public float jumpPower = 150f;

    public bool grounded;
    public bool doubleJump;
    public int curHealth;
    public int maxHealth = 3;
    public bool wallSliding;
    public bool facingRight = true;

    private Rigidbody2D rb;
    private Animator anim;
    private gameMaster gm;
    public Transform wallCheckPoint;
    public bool wallCheck;
    public LayerMask wallLayerMask;

    // Start is called before the first frame update
    void Start()
    {
        //Get Components
        rb = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        curHealth = maxHealth;
        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<gameMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        //Animator
        anim.SetBool("Grounded", grounded);
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));

        //Movement
        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            facingRight = false;
        }

        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
            facingRight = true;
        }

        //Jump
        if (Input.GetButtonDown("Jump") && !wallSliding)
        {
            if (grounded)
            {
                rb.AddForce(Vector2.up * jumpPower);
                doubleJump = true;
            }
            else
            {
                if (doubleJump)
                {
                    doubleJump = false;
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    rb.AddForce(Vector2.up * jumpPower / 1.75f);
                }
            }
        }

        if(curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

        if(curHealth <= 0)
        {
            Die();
        }

        if (!grounded)
        {
            wallCheck = Physics2D.OverlapCircle(wallCheckPoint.position, 0.1f, wallLayerMask);
            if (facingRight && Input.GetAxis("Horizontal") > 0.1f || !facingRight && Input.GetAxis("Horizontal") < 0.1f)
            {
                if (wallCheck)
                {
                    HandleWallSliding();
                }
            }

        }

        if(wallCheck == false || grounded)
        {
            wallSliding = false;
        }
    }

    void HandleWallSliding()
    {
        rb.velocity = new Vector2(rb.velocity.x, -0.7f);
        wallSliding = true;
        if (Input.GetButtonDown("Jump"))
        {
            if (facingRight)
            {
                rb.AddForce(new Vector2(-2, 1) * jumpPower);
            }
            else
            {
                rb.AddForce(new Vector2(2, 1) * jumpPower);
            }
        }
    }

    void FixedUpdate()
    {
        Vector3 easeVelocity = rb.velocity;
        easeVelocity.y = rb.velocity.y;
        easeVelocity.z = 0.0f;
        easeVelocity.x *= 0.75f;

        float h = Input.GetAxis("Horizontal");

        //Fake Friction, Ease the x speed 
        if (grounded)
        {
            rb.velocity = easeVelocity;
        }

        //Movement
        if (grounded)
        {
            rb.AddForce((Vector2.right * speed) * h);
        }
        else
        {
            rb.AddForce((Vector2.right * speed / 2) * h);
        }

        //Limit to speed
        if (rb.velocity.x > maxSpeed)
        {
            rb.velocity = new Vector2(maxSpeed, rb.velocity.y);
        }

        if (rb.velocity.x < -maxSpeed)
        {
            rb.velocity = new Vector2(-maxSpeed, rb.velocity.y);
        }
    }

    //What Happends to the Character when it dies
    void Die()
    {
        if (PlayerPrefs.HasKey("Highscore"))
        {
            if(PlayerPrefs.GetInt("Highscore") < gm.points)
            {
                PlayerPrefs.SetInt("Highscore", gm.points);
            }
            else
            {
                PlayerPrefs.SetInt("Highscore", gm.points);
            }
        }
        SceneManager.LoadScene("Gameover");
    }

    //Damage that character takes and plays red flash animation
    public void Damage(int dmg)
    {
        curHealth -= dmg;
        gameObject.GetComponent<Animation>().Play("RedFlash");
    }

    //Calls in the knockback of the character when its hurts with the spikes
    public IEnumerator Knockback(float knockDur, float knockbackPwr, Vector3 knockbackDir)
    {
        float timer = 0;
        

        while( knockDur > timer)
        {
            timer += Time.deltaTime;
            rb.AddForce(new Vector3(knockbackDir.x * -100, knockbackDir.y + knockbackPwr, transform.position.z));

        }

        yield return 0;
    }

    //On Collider to Activate when it touches the coins
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Coin"))
        {
            Destroy(col.gameObject);
            gm.points += 1;
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("movingplat"))
            this.transform.parent = col.transform;
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("movingplat"))
            this.transform.parent = null;
    }
}
