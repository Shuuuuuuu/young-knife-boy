﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameMaster : MonoBehaviour
{
    //Variables
    public int points;
    public int highscore = 0;

    public Text pointsText;
    public Text InputText;

    void Start()
    {
        if (PlayerPrefs.HasKey("Points"))
        {
            if (SceneManager.GetActiveScene().name == "Main")
            {
                PlayerPrefs.DeleteKey("Points");
                points = 0;
            }
            else
            {
                points = PlayerPrefs.GetInt("Points");
            }
        }

        if (PlayerPrefs.HasKey("Highscore"))
        {
            highscore = PlayerPrefs.GetInt("Highscore");
        }
    }
    void Update()
    {
        pointsText.text = ("Points: " + points);
    }
}
